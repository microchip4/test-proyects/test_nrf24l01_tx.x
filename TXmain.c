/*
 * File:   TXmain.c
 * Author: pic18fxx.blogspot.com
 * 
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
//#include <pic18f4550.h>
//#include <pic18f46k22.h>
#include "Alteri.h"
#include <spi.h>
#include "nrf24l01.h"
#include "hd44780.h"

void main(void) {
    unsigned char i;
    char buffer1[20];
    unsigned char bufferTX[32];
    ADCON0=0x00; //all digital
    ADCON1=0x00; //all digital
    ADCON1bits.PCFG=0x0F;
    //__delay_ms(100);  // Allow time for Vdc to settle on GLCD
    TRISB = 0x00;       //Set data port  B as output
    TRISC = 0x00;
    TRISD = 0x00;       // Set control port D as output
    /*
    OSCCON = 0b01110000;
    OSCTUNEbits.PLLEN = 1; // turn on the PLL 64 MHz
    ANSELA = 0; ANSELB = 0; ANSELC = 0; ANSELD = 0; ANSELE = 0;
    PORTA  = 0; PORTB  = 0; PORTC  = 0; PORTD  = 0; PORTE  = 0;
    TRISA  = 0; TRISB  = 0; TRISC  = 0; TRISD  = 0; TRISE  = 0;
     * */
    CloseSPI();
    OpenSPI(SPI_FOSC_64, MODE_00, SMPEND);
    Lcd_Init();
    Lcd_Command(LCD_CLEAR);
    Lcd_Command(LCD_CURSOR_OFF);
    Lcd_ConstText(1, 1, "nRF24l01 TX Mode");

    NRF24L01_Init(TX_MODE, 0x40); 
    while(1){
        bufferTX[0] = 1;   
        for(i = 0; i < 8; i++){
            NRF24L01_SendData(bufferTX); 
            sprintf(buffer1, "Sending: %03d", bufferTX[0]);
            Lcd_Text(2, 3, buffer1);
            bufferTX[0] = bufferTX[0] << 1;
            delay_ms(300);  
        }
        bufferTX[0] = 128;
        for(i = 0; i < 6; i++){
            bufferTX[0] = bufferTX[0] >> 1;
            NRF24L01_SendData(bufferTX); 
            sprintf(buffer1, "Sending: %03d", bufferTX[0]);
            Lcd_Text(2, 3, buffer1);
            delay_ms(300);  
        }
    }
    while(1){}
}